<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PeopleCountHome extends CI_Controller {
	var $data;
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('PeopleCountModel','hv');
		$this->data['controller'] = 'PeopleCountHome';
		$this->data['home'] = 'People Count';
	}
/**
 * dashboard functionallity
 */
	public function index()
	{
		$data = [
			'active' => 'peopleCount_dash',
			'get_default_device' => $this->hv->get_default_device(),
			'today_data' => $this->hv->get_today_data(),
			'total_device' => $this->hv->get_total_device(),
			'g_actual_data' => $this->hv->get_actual_data(),
			'threshold_limit' => $this->hv->get_threshold_limit(),
			'threshold_data' => $this->hv->get_threshold_data('5'),
			'last_five_data' => $this->hv->capture_list('5'),
			'additional_js'=> ['raphael.min.js','morris.min.js'],
			'additional_css' => ['morris.css']
		];

		$data = array_merge($data,$this->data);
		$this->load->view('peopleCount/home',$data);
	}

	/**
	 * capture list
	 */
	public function capture_list() {
		$capture_list = $this->hv->capture_list();
		$data = [
			'title'	=> 'Capture List',
			'action' => 'List',
			'active' => 'peopleCount_dash',
			'list_data' => $capture_list,
			'additional_css' => ['custom_style.css']
			];
			$data = array_merge($data,$this->data);
		$this->load->view('peopleCount/capture_list',$data);
	}

	/**
	 * method for add all basic components needed
	 * basic configuration settings field add function
	 */
	function common_settings(){
		$key_data = $this->hv->select_key_data();
		$data = [
			'title'	=> 'Common Settings',
			'action' => 'List',
			'active' => 'peopleCount_comm',
			'list_data' => $key_data,
			'additional_js' => ['bootstrap-tagsinput.js'],
			'additional_css' => ['bootstrap-tagsinput.css','custom_style.css']
			];
			$data = array_merge($data,$this->data);
		$this->load->view('peopleCount/common_settings',$data);
	}

	/**
	 * save common setings
	 * @param parametername,value
	 */
	public function save_common_settings () {

		$post_data = $this->input->post();
		$this->hv->save_common_settings($post_data);
	}

	/**
	 * add screen for adding camera
	 */
	public function add_camera($edit = null) {
		$location = explode(',',$this->hv->get_settings('Location'));
		$location_type =explode(',',$this->hv->get_settings('Location_type'));
		$brand =explode(',',$this->hv->get_settings('Brand'));
		$protocol =explode(',',$this->hv->get_settings('Protocol'));
		$data = [
			'active' => 'peopleCount_cam_add',
			'title'	=> 'Camera',
			'action' => 'Add',
			'location' => $location,
			'location_type' => $location_type,
			'brand' => $brand,
			'protocol' => $protocol,
		];
		if($edit != null) {
			$data['fetch_data'] = $this->hv->select_camera($edit);
		}
		$data = array_merge($data,$this->data);
		$this->load->view('peopleCount/add_camera',$data);
	}

	/**
	 * save camera fields
	 * @param camera_name,location,brand,location_type,protocol
	 */
	public function save_camera() {
		$post_data = $this->input->post();
		$return  = $this->hv->save_camera($post_data);
		if($return==true) {
			redirect(base_url('/PeopleCountHome/camera'));
		}else{
			$this->load->view('peopleCount/add_camera',$data);
		}
	}

	/**
	 * list screen for camera
	 */
	public function camera() {
		$list_data = $this->hv->select_camera();
		$data = [
			'active' => 'peopleCount_cam',
			'title'	=> 'Camera',
			'action' => 'List',
			'list_data' => $list_data
		];
		$data = array_merge($data,$this->data);
		$this->load->view('peopleCount/camera',$data);
	}

	/**
	 * delete camera
	 */
	public function delete_camera($id) {
		$this->hv->delete_camera($id);
		redirect(base_url('/PeopleCountHome/camera'));
	}

	/***************************************report module start**********************************/
	public function select_people_count() {
		$data = [
			'active' => 'people_count_report',
			'title'	=> 'People Count',
			'action' => 'Report',
		];
		$data = array_merge($data,$this->data);
		$this->load->view('peopleCount/select_people_count',$data);
	}
	public function people_count_report() {
		$post = $this->input->post();
		$get_data = $this->hv->people_count_report($post);
		$data = [
			'active' => 'people_count_report',
			'title'	=> 'People Count',
			'action' => 'Report',
			'report_data' => $get_data
		];
		$data = array_merge($data,$this->data);
		$this->load->view('peopleCount/people_count_report',$data);
	}
}
