<?php
/**
 * NOTE: Entry point of API
 * API for getting data from devices.
 */
class ApiInOut extends CI_Controller {

  public function __construct() {
		parent::__construct();
    $this->load->library('session');
    $this->load->model('apiInOut_model', 'api');
  }

  /**
   * login authentication
   * @return token,code,message
   */
  public function get_device_data() {
    $output = [
      'code' => 500,
      'success' => false,
      'content' => [
        'message' => 'not processed'
      ]
    ];
    try {
			parse_str($_SERVER['QUERY_STRING'], $_GET);
			$data = $this->api->get_device_data($_GET);
      if ($_GET != null) {

        $output['success'] = true;
        $output['code'] = 200;
        $output['content']['data'] = 'Data Received';
        $output['content']['message'] = 'Success';


      } else {
        throw new Exception('null', 400);
      }
    } catch (Exception $error) {
      $output['success'] = false;
      $output['code'] = $error->getCode();
      $output['content']['message'] = $error->getMessage();
    }
    $output['content']['code'] = $output['code'];
    return $this->output
                ->set_content_type('application/json')
                ->set_status_header($output['code'])
                ->set_output(json_encode($output['content']));
	}

	/**
	 * set device_data
	 */
	public function set_device_data() {
		$output = [
      'code' => 500,
      'success' => false,
      'content' => [
        'message' => 'not processed'
      ]
    ];
    try {
			parse_str($_SERVER['QUERY_STRING'], $_GET);
			$data = $this->api->set_device_data($_GET);
      if ($data == true) {

        $output['success'] = true;
        $output['code'] = 200;
				$output['content']['data'] = 'Data Received';
        $output['content']['message'] = 'Success';

      } else {
        throw new Exception('null', 400);
      }
    } catch (Exception $error) {
      $output['success'] = false;
      $output['code'] = $error->getCode();
      $output['content']['message'] = $error->getMessage();
    }
    $output['content']['code'] = $output['code'];
    return $this->output
                ->set_content_type('application/json')
                ->set_status_header($output['code'])
                ->set_output(json_encode($output['content']));
	}


}
?>
