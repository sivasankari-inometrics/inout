<?php
class InOutHomeModel extends CI_Model{
  function __construct() {

		parent::__construct();
		$this->load->database();
		$this->device = $this->db->get_where('inout_settings',array('key_field'=>'default_device'))->row()->key_value;
		//$this->today = date('Y-m-d');
		$this->today = '2019-06-17';
	}

	//***********************common settings******************************** */
	/**
	 * select all data from  inout_settings
	 */
	public function select_key_data () {
		$data = $this->db->get_where('inout_settings',array())->result();
		if(count($data)>0) {
			return $data;
		}
	}

	/**
	 * save common settings value
	 * @param keyfield,keyvalue
	 */
	public function save_common_settings($post_data) {

		$data = [
			'key_value'=> $post_data['key_value']
		];
		$this->db->where('key_field',$post_data['key_field']);
		$this->db->update('inout_settings',$data);
	}

	//********************************home Page ***************************************************** */
		public function get_total_device() {
			$result = $this->db->query("SELECT count(DISTINCT name) as camera_count,count(DISTINCT location) as location_count FROM inout_device")->result();
			return $result[0];
		}

		public function get_total_inout() {
			$result = $this->db->query("SELECT in_count,out_count,created_time from inout_readings ORDER BY id DESC LIMIT 1")->result();
			return $result[0];
		}

		public function get_chart_inout($from=null, $to=null ,$device=null) {
			if($to==null) {
				$to=1;
			}
			if($device == null) {
				$device = $this->device;
			}
			$to_timest = date('H:00:00',strtotime(date('H:00:00'))-60*60*$from);
			$c_time = strtotime(date('H:00:00'))-60*60*$to;
			$from_timest = date('H:00:00',$c_time);
			$resul_first =	$this->db->order_by('id', 'ASC')->get_where('inout_readings',array('device_id'=>$this->device,'time(created_time)>='=>$from_timest,'time(created_time)<='=>$to_timest,'date(created_time)<='=>$this->today,'device_id'=>$device),1)->result();

			$resul_last =	$this->db->order_by('id', 'DESC')->get_where('inout_readings',array('device_id'=>$this->device,'time(created_time)>='=>$from_timest,'time(created_time)<='=>$to_timest,'date(created_time)<='=>$this->today,'device_id'=>$device),1)->result();
			return $data = [
				'in_count' => ($resul_last!=null && $resul_first!=null)?$resul_last[0]->in_count-$resul_first[0]->in_count:0,
				'out_count' => ($resul_last!=null && $resul_first!=null)?$resul_last[0]->out_count-$resul_first[0]->out_count:0,
				'from_time' => date('h',strtotime($from_timest)),
				'to_time' => date('h a',strtotime($to_timest)),
			];

		}

		public function inside_people() {
			$result = $this->db->query("SELECT (in_count-out_count) as inside_count from inout_readings ORDER BY id DESC LIMIT 1")->result();
			return $result[0];
		}
	//*************************************camera module********************************** */

	/**
	 * list for all inout_settings table fields
	 * table - inout_settings
	 * @param key_field
	 * @return key_value
	 */
	public function get_settings($field) {
		$data = $this->db->get_where('inout_settings',array())->result();
		if(count($data)>0) {
			foreach($data as $dt) {
				if($field==$dt->key_field) {
				return $dt->key_value;
				}
			}
		}
	}

	/**
	 * save camera
	 * table - inout_device
	 */
	public function save_camera($post_data) {
		if($post_data["id"]!=null) {
			$this->db->where('id',$post_data["id"]);
			$query = $this->db->update('inout_device', $post_data);
		}else {
			$query = $this->db->insert('inout_device', $post_data);
			// for update all other readings for current device
			$data['device_id'] = $post_data['name'];
			$query = $this->db->insert('people_count_lastreading', $data);
		}
    return $query ? true : false;
	}
	/**
	 * select camera_data
	 */
	public function select_camera($id=null) {
		if($id!=null) {
			$data = $this->db->get_where('inout_device',array('id'=>$id))->row();
		}else {
			$data = $this->db->get_where('inout_device',array())->result();
		}
		if($data!=null) {
				return $data;
		}
	}

	/**
	 * delete camera
	 */
	public function delete_camera($id) {

		$this->db->where('id',$id);
		$this->db->delete('inout_device');
	}
}
