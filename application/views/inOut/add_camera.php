<?php
include('application/views/include/header.php');
include('application/views/include/sidebar.php');
?>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
	<div class="pcoded-wrapper">
		<div class="pcoded-content">
			<div class="pcoded-inner-content">
					<!-- [ breadcrumb ] start -->
				<div class="page-header">
					<div class="page-block">
						<div class="row align-items-center">
							<div class="col-md-12">
								<div class="page-header-title">
										<h5 class="m-b-10"><?= $title ?></h5>
								</div>
								<ul class="breadcrumb">
										<li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
										<li class="breadcrumb-item"><a href="javascript:"><?= $home.' / '.$title ?></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
					<!-- [ breadcrumb ] end -->
				<div class="main-body">
					<div class="page-wrapper">
							<!-- [ Main Content ] start -->
						<div class="row">
							<div class="col-sm-12">
								<div class="card">
									<div class="card-header">
											<h5><?= $title.' - '.$action ?></h5>
									</div>
									<div class="card-body">
										<div class="row">
											<div class="col-md-6">
												<form action="<?=base_url($controller.'/save_camera') ?>" method="post">
													<div class="form-group">
															<label for="exampleInputEmail1">Camera Name</label>
															<input type="text" class="form-control"   placeholder="Camera Name" name="name" value="<?= isset($fetch_data->name)?$fetch_data->name:'' ?>">
													</div>

													<div class="form-group">
														<label >Brand</label>
														<select class="form-control" name="brand">
														<option>-----------------------------------------select-------------------------------------------</option>
														<?php foreach($brand as $value) { ?>
															<option <?= isset($fetch_data->brand)? $fetch_data->brand==$value ? 'selected' : '' : '' ?>><?= $value ?></option>
														<?php } ?>
														</select>
													</div>
													<div class="form-group">
														<label >Protocol</label>
														<select class="form-control" name="protocol">
														<option>-----------------------------------------select-------------------------------------------</option>
														<?php foreach($protocol as $value) { ?>
															<option <?= isset($fetch_data->protocol)? $fetch_data->protocol==$value ? 'selected' : '' : '' ?>><?= $value ?></option>
														<?php } ?>
														</select>
													</div>
													<div class="form-group">
														<label >Location</label>
														<select class="form-control" name="location" >
														<option>-----------------------------------------select-------------------------------------------</option>
														<?php foreach($location as $value) { ?>
															<option <?= isset($fetch_data->location)? $fetch_data->location==$value ? 'selected' : '' : '' ?>><?= $value ?></option>
														<?php } ?>
														</select>
													</div>
													<div class="form-group">
														<label >Location Type</label>
														<select class="form-control" name="location_type" >
														<option>-----------------------------------------select-------------------------------------------</option>
														<?php foreach($location_type as $value) { ?>
															<option <?= isset($fetch_data->location_type)? $fetch_data->location_type==$value ? 'selected' : '' : '' ?>><?= $value ?></option>
														<?php } ?>
														</select>
													</div>
													<input type="hidden" name="id" value="<?=isset($fetch_data->id)?$fetch_data->id:'' ?>">
													<button  class="btn btn-primary" id="button">Submit</button>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
							<!-- [ Main Content ] end -->
					</div>
				</div>
			</div>
		</div>
	</div>
    </div>
	<!-- [ Main Content ] end -->
	<?= include('application/views/include/footer.php'); ?>
