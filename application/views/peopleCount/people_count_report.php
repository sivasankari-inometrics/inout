<?php
include('application/views/include/header.php');
include('application/views/include/sidebar.php');
?>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
	<div class="pcoded-wrapper">
		<div class="pcoded-content">
			<div class="pcoded-inner-content">
					<!-- [ breadcrumb ] start -->
				<div class="page-header">
					<div class="page-block">
						<div class="row align-items-center">
							<div class="col-md-12">
								<div class="page-header-title">
										<h5 class="m-b-10"><?= $title ?></h5>
								</div>
								<ul class="breadcrumb">
										<li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
										<li class="breadcrumb-item"><a href="javascript:"><?= $home.' / '.$title ?></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
					<!-- [ breadcrumb ] end -->
				<div class="main-body">
					<div class="page-wrapper">
							<!-- [ Main Content ] start -->
						<div class="row">
							<div class="col-sm-12">
								<div class="card">
									<div class="card-header">
											<h5><?= $title.' - '.$action ?></h5>
									</div>
									<div class="card-body">
										<div class="row">
											<div class="col-md-12">
											<div class="table-responsive">
                                                <table class="table table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Device Id</th>
                                                            <th>People Count</th>
                                                            <th>Co2</th>
                                                            <th>Humidity</th>
																														<th>Temperature</th>
																														<th>Date</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
																											<?php
																											$i=0;
																											 foreach($report_data as $rdata) { ?>
                                                        <tr>
                                                            <th scope="row"><?= $i++ ?></th>
                                                            <td><?= $rdata->device_id ?></td>
                                                            <td><?= $rdata->people_count ?></td>
                                                            <td><?= $rdata->co2 ?></td>
                                                            <td><?= $rdata->humidity ?></td>
                                                            <td><?= $rdata->temperature ?></td>
                                                            <td><?= $rdata->created_time ?></td>
                                                        </tr>
																													<?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
							<!-- [ Main Content ] end -->
					</div>
				</div>
			</div>
		</div>
	</div>
    </div>
	<!-- [ Main Content ] end -->
	<?= include('application/views/include/footer.php'); ?>
